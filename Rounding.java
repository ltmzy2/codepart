  
        //数字操作
  
        //setScale 方法来设置精确度，同时使用 RoundingMode.HALF_UP 表示 使用最近数字舍入法则来近似计算
        double f = 31.5585;
        BigDecimal b = new BigDecimal(f);
        double f1 = b.setScale(2, RoundingMode.HALF_EVEN).doubleValue();
        
        System.out.println(f1);

        //#.00 表示两位小数 #.0000四位小数 以此类推…
        DecimalFormat df = new DecimalFormat("#.00");
        String format = df.format(f);
        
        System.out.println(format);

        //%.2f %. 表示 小数点前任意位数 2 表示两位小数 格式后的结果为f 表示浮点型。
        String format1 = String.format("%.2f", f);
        
        System.out.println(format1);