    
    //感觉写的不错的代码
    /**
     * @param mileage
     * @param starCityId
     * @param endCityId
     * @return com.shunlu.cloud.entity.vo.SLResponse
     * @author: Jy  2019/7/16 14:20
     */
    @PostMapping("/calculate")
    @ApiOperation(value = "计算订单价格", notes = "计算订单价格")
    public SLResponse calculate(
            @RequestParam(value = "mileage", required = true) @ApiParam(value = "距离(公里)", required = true) Double mileage,
            @RequestParam(value = "starCityId", required = true) @ApiParam(value = "出发城市id", required = true) String starCityId,
            @RequestParam(value = "endCityId", required = true) @ApiParam(value = "到达城市id", required = true) String endCityId) {

        //专线计算后剩余没有开通专线的板车类型
        List<Integer> typeList = new ArrayList<>();
        //结果标记
        Map<String, Object> mark = new HashMap<>();
        //计算结果
        List<Map<String, Object>> result = new ArrayList<>();
        //响应结果
        Map<String, Object> response = new HashMap<>();
        //直发价
        Map<String, Object> direct = new LinkedHashMap<>();
        //顺路价
        Map<String, Object> baytheway = new LinkedHashMap<>();
        //专线价
        Map<String, Object> exclusiveLineMap = new LinkedHashMap<>();

        for ( int type = 1; type < 5; type++ ) {
            LExclusiveLine exclusiveLine = exclusiveLineReadOnlyService.selectExclusiveLinePrice(starCityId, endCityId, type);
            if (exclusiveLine != null && mileage <= exclusiveLine.getSetMileage()) {
                exclusiveLineMap.put("exclusiveLinePrice_" + type, exclusiveLine.getPrice());
                baytheway.put("baythewayPrice_" + type, 0);
            } else {
                exclusiveLineMap.put("exclusiveLinePrice_" + type, 0);
                typeList.add(type);
            }
        }

        boolean isShortDistanceCity = true;
        for ( int type = 1; type < 5; type++ ) {
            //未开通150内短途不再计算
            if (mileage < 150) {
                String openCity = shortDistanceCityReadOnlyService.getOpenCity(starCityId);
                if (!"true".equals(openCity)) {
                    isShortDistanceCity = false;
                    break;
                }
            }
            //读取当前板车类型配置
            LPriceList config = new LPriceList();
            LPriceList priceList = priceListReadOnlyService.getPriceList(starCityId, type);
            if (priceList != null) {
                config = priceList;
            } else {
                config = customerConfigReadOnlyService.setCustomerConfig(config, type);
            }
            double myDistance = config.getMyDistance();
            //如果当前板车类型存在,按照顺路计算
            if (mileage < myDistance) {
                //短途
                for ( Integer truckType : typeList ) {
                    if (truckType == type) {
                        //顺路
                        baytheway.put("baythewayPrice_" + truckType, Paylist.canculateFrieght(
                                config.getsCountingFee(), config.getsStartingPrice(), mileage,
                                config.getCostOfPlatform(), (double) config.getStartMileage(), myDistance, 0, 0)
                                .getFreight());
                    }
                }
                //直发
                direct.put("directPrice_" + type, Paylist.canculateFrieght(
                        config.getdCountingFee(), config.getdStartingPrice(), mileage,
                        config.getCostOfPlatform(), (double) config.getStartMileage(), myDistance, 0, 0)
                        .getFreight());
            } else {
                //长途
                for ( Integer truckType : typeList ) {
                    if (truckType == type) {
                        //顺路
                        baytheway.put("baythewayPrice_" + truckType, Paylist.canculateFrieght(
                                config.getsCountingFeeCrosscity(), config.getsStartingPriceCrosscity(), mileage,
                                config.getLongCostOfPlatform(), (double) config.getStartMileage(), myDistance, 0, 0)
                                .getFreight());
                    }
                }
                //直发
                direct.put("directPrice_" + type, Paylist.canculateFrieght(
                        config.getdCountingFeeCrosscity(), config.getdStartingPriceCrosscity(), mileage,
                        config.getLongCostOfPlatform(), (double) config.getStartMileage(), myDistance, 0, 0)
                        .getFreight());
            }
        }
        result.add(exclusiveLineMap);
        result.add(baytheway);
        result.add(direct);
        mark.put("isHasExclusiveLine", typeList.size() != 4);
        mark.put("isShortDistanceCity", isShortDistanceCity);
        response.put("mark", mark);
        response.put("list", result);
        return new SLResponse("200", "计算成功", response);
    }
