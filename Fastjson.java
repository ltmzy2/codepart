    //springboot启动注册fastjson
    @Bean
    public HttpMessageConverters fastjsonHttpMessageConverter() {
        FastJsonHttpMessageConverter messageConverter = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
        messageConverter.setFastJsonConfig(fastJsonConfig);
        return new HttpMessageConverters(messageConverter);
    }