import org.apache.commons.lang3.StringUtils;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
/**
 * @program: **
 * @author: Jy
 * @create: 2019-06-10 11:19
 **/
public class LogUtil {


    private static final String ADD = "添加了";
    private static final String UPDATE = "修改了";

    /**
     * @param model    修改的数据类
     * @param log      日志类
     * @param operater 操作者
     * @param operaterType   ADD UPDATE
     * @return void
     * @author: Jy  2019/6/10 14:43
     */
    public static void setModelToLog(Object model, Object log, String operater, String operaterType) {
        Class<?> modelClass = model.getClass();
        StringBuilder content = new StringBuilder(operater + operaterType + modelClass.getSimpleName() + ":");;
        for ( Method modelMethod : modelClass.getDeclaredMethods() ) {
            modelMethod.setAccessible(true);
            String modelFieldName = modelMethod.getName();
            if (modelFieldName.startsWith("get")) {
                Object value = null;
                try {
                    value = modelMethod.invoke(model);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (value != null) {
                    Class<?> logClass = log.getClass();
                    for ( Method declaredMethod : logClass.getDeclaredMethods() ) {
                        String methodName = declaredMethod.getName();
                        if (methodName.equals("setOperationRecord")) {
                            for ( Class<?> parameterType : declaredMethod.getParameterTypes() ) {
                                //当前方法为:setOperationRecord
                                if ("string".equalsIgnoreCase(parameterType.getSimpleName())) {
                                    //追加属性名和属性值
                                    String initcap = initcap(modelFieldName.substring(3));
                                    content.append(initcap + "为[" + value + "];");
                                    try {
                                        declaredMethod.invoke(log, content.toString());
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    } catch (InvocationTargetException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

    }

    //把字段第一个字母大写
    public static String initcap(String field) {
        if (StringUtils.isEmpty(field)) {
            return field;
        } else if (field.length() == 1) {
            return field.toUpperCase();
        }
        return field.substring(0, 1).toUpperCase() + field.substring(1);
    }

}
