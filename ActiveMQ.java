
//延迟消息推送配置
@Component
public class CustomerProducer {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    public void send(Destination destination, final Map<String, String> message) {
        jmsMessagingTemplate.convertAndSend(destination, message);
    }

    public void sendMessage(String queueName, Map<String, String> jsonMessage) {
        Destination destination = new ActiveMQQueue(queueName);
        send(destination, jsonMessage);
    }


    @Autowired
    private JmsTemplate jmsTemplate;

    /**
     * 发送延迟消息
     *
     * @param queue 队列名
     * @param map   参数
     * @param delay 延迟时间[毫秒]
     * @return void
     * @author: Jy  2019/6/28 15:51
     */
    public void sendDelay(String queue, Map<String, String> map, long delay) {
        Destination destination = new ActiveMQQueue(queue);
        jmsTemplate.convertAndSend(destination, map, message -> {
            message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, delay);
            return message;
        });
    }
}
